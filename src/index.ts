import express from 'express';
import mongoose from 'mongoose'
import { json } from 'body-parser';
import { apiRouter } from './routes/api';
import { mongooseData } from './config/mongoose'
import { serverData } from './config/server'

const app = express();
app.use(json())
app.use('/api', apiRouter)

mongoose.connect(`mongodb://${mongooseData.host}:${mongooseData.port}/${mongooseData.database}`, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true
}, () => {
  console.log(`connected to ${mongooseData.database} on ${mongooseData.host}:${mongooseData.port}`)
})

app.listen(serverData.port, () => {
  console.log(`server is started on port ${serverData.port}`)
})