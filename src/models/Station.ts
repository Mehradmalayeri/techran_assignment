import * as mongoose from 'mongoose';

interface IStation extends mongoose.Document {
    name: string;
    latitude: string;
    longitude: string;
    company: mongoose.Schema.Types.ObjectId;
}

const companySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    latitude: {
        type: String,
        required: true
    },
    longitude: {
        type: String,
        required: true
    },
    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Company',
        required: true
    }
})

const Station = mongoose.model<IStation>('Station', companySchema)


export { Station }