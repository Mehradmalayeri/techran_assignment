import * as mongoose from 'mongoose'

interface ICompany extends mongoose.Document {
    name: string;
    parent?: ICompany['_id'];
    ancestors: ICompany['_id'][];
}

const companySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    parent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Company',
        default: null
    },
    ancestors: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Company",
        index: true
    },
    ]
})

const Company = mongoose.model<ICompany>('Company', companySchema)


export { Company }